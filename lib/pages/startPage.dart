import 'package:flutter/material.dart';
import 'package:flutter_app/router.dart';

class StartPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: new AppBar(
        title: Text('Welcome' , textAlign: TextAlign.center, style: TextStyle(color: Colors.black , fontSize: 20.0),),
      ),
      body: Center(
        child: ButtonTheme(
          height: 60.0,
          minWidth: 150.0,
          child: RaisedButton(
            elevation: 20.0,
            child: Text(
              'CLICK HERE TO SEE MAGIC',
              textAlign: TextAlign.center,
              style: TextStyle( color: Colors.black , ),
            ),
    shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(100.0),
    side: BorderSide(color: Colors.white,width: 2.0,),


            ),
            color: Colors.green[100],
            onPressed: (){
              Navigator.pushNamed(context, homepage);
            },

        ),
      )
      )
    );
  }
}