import 'package:flutter/material.dart';
import 'package:flutter_app/Themes/themes.dart';
import 'package:flutter_app/models/country.dart';


class Showcities extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
        home: Display()
    );
  }
}

class Display extends StatefulWidget{
  @override
  State createState() => new list();

}

class list extends State<Display>{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(
      title: Text('Current cities' , textAlign: TextAlign.center,
        style: normalStyle.copyWith(color: Colors.black),
      ),

      ),
      body: new ListView.separated(

          itemBuilder: (BuildContext context  , int index) => _buildBody(context , index),
          separatorBuilder: buildSeparator,
          itemCount: countries.length,
      ),

    );

  }
}

Widget _buildBody(BuildContext context , int index){

  return new Card(
   child: Container(
     height: 30.0,
     child: Text(
       countries[index].countryName,
       textAlign: TextAlign.center,
       style: appBarTextStyle,
     ),

   ),
  );
}

Widget buildSeparator(context, index) {
  return Row(
    children: <Widget>[
      Expanded(
        child: Divider(),
      ),
      Text('wow'),
      Expanded(
        child: Divider(),
      ),
    ],
  );
}