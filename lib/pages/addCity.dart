import 'package:flutter/material.dart';
import 'package:flutter_app/Themes/themes.dart';
import 'package:flutter_app/models/country.dart';

class AddCity extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Wanna add cities',
      home: Display(),
    );

  }
}

class Display extends StatelessWidget{
  TextEditingController _controller = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text(
          'Wanna add cities',
          style: normalStyle,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Column(
          children: <Widget>[
              TextField(

                decoration: InputDecoration(
                  labelText: 'Enter the country'
                ),
                controller: _controller,




              ),
            SizedBox(
              height: 30.0,
            ),
            ButtonTheme(
              height: 60.0,
              minWidth: 150.0,
              child: RaisedButton(
                elevation: 12.0,
                child: Center(
                  child: Text(
                    'Add City' , style: TextStyle(color: Colors.black , fontSize: 24.0),
                  ),
                ),
                onPressed: (){
                  countries.add(CountryModel(countryName: _controller.text , imagePath: 'images/newImage.png'));
                },


      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
        side: BorderSide(color: Colors.white,width: 2.0,),

      ),
              ),
            ),

          ],
        ),
      ),
    );

  }
}