import 'package:meta/meta.dart';

class CountryModel{
  final String countryName;
  final String imagePath;

  CountryModel({
   @required this.countryName,
   @required this.imagePath,
});


}


List<CountryModel> countries = [
  CountryModel(countryName: 'Kolkata', imagePath: 'images/kolkata.jpeg'),
  CountryModel(countryName: 'London', imagePath: 'images/london.jpeg'),
];

int checkIfPresent( String country){
  for ( int i =0 ; i < countries.length ; i++){
    if ( countries[i].countryName == country)
      return 1;
  }
  return 0;
}