import 'package:flutter/material.dart';
import 'package:flutter_app/Themes/themes.dart';
import 'package:flutter_app/router.dart';




class NavBar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              height: 80,
              decoration: BoxDecoration(
                borderRadius:   BorderRadius.circular(5.0),
                shape: BoxShape.rectangle
              ),
              child: Center(
                child: ButtonTheme(
                  height: 60.0,
                  minWidth: 150.0,
                  child: RaisedButton(

                    elevation: 10.0,
                    color: Colors.green[100],
                    onPressed: (){
                      Navigator.pushNamed(context, addCity);
                    },

                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Add a city',
                      textAlign: TextAlign.center,
                      semanticsLabel: 'add a city',
                      style: normalStyle.copyWith(
                        color: Colors.black,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100.0),
                            side: BorderSide(color: Colors.white,width: 2.0,),

                    ),
                  ),


                ),

              ),



              ),
            ),






        ],
      ),
    );


  }
}