
import 'package:flutter_app/models/country.dart';

abstract class WeatherListingEvent{

}


class CountrySelectedEvent extends WeatherListingEvent{
  final CountryModel countryModel;

  CountrySelectedEvent({this.countryModel})
      :assert (countryModel!=null);

}