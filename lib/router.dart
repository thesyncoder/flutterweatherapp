import 'package:flutter/material.dart';
import 'package:flutter_app/pages/addCity.dart';
import 'package:flutter_app/pages/cities.dart';
import 'package:flutter_app/pages/homepage.dart';
import 'package:flutter_app/pages/startPage.dart';
const String citiesRoute = '/cities';
const String addCity= '/addCity';
const String homepage ='/homePage';


class Router{

  static Route<dynamic> generateRoute( RouteSettings settings){
    // settings contains info like -
    // route name and arguments
    switch(settings.name){
      case citiesRoute:
        return MaterialPageRoute( builder: (_) => Showcities());
        break;
      case addCity:
        return MaterialPageRoute( builder: (_)=> AddCity());
        break;
      case homepage:
        return MaterialPageRoute( builder: (_) => Homepage());
        break;
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')),
            ));
        break;

    }

  }

}