
import 'package:flutter_app/Themes/themes.dart';
import 'package:flutter_app/models/api_model.dart';
import 'package:flutter_app/models/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/services/WeatherApiProvider.dart';


class HorizontalBar extends StatefulWidget {
  @override
  HorizontalBarState createState() {
    return new HorizontalBarState();
  }
}

class HorizontalBarState extends State<HorizontalBar>{

  @override
  Widget build(BuildContext context) {

      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Container(
          height: 400.0,
          width: 300.0,
          color: Colors.white,
          child: ListView.separated(
            separatorBuilder: buildSeparator,
            itemBuilder: buildItem,
            itemCount: countries.length,
            scrollDirection: Axis.horizontal,
          ),
        ),
      );

  }

  Widget buildItem(context, index) {
    return Card(
      color: Colors.amber[50],
      elevation: 8.0,
      child: Padding(
        padding: EdgeInsets.only(top:30.0),
        child: InkWell(
          onTap: () {

          },

          child: Column(
            children: <Widget>[

              Container(
                width: 150.0,
                height: 200.0,
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                    color: Colors.grey,
                    blurRadius: 5.0,
                  ),],

                  shape: BoxShape.rectangle,

                  image: DecorationImage(

                    image: AssetImage(countries[index].imagePath),
                    fit: BoxFit.fill,


                  ),
                ),
                margin: EdgeInsets.symmetric(horizontal: 16.0),
              ),
              SizedBox(height: 20.0),
              Container(
                padding: EdgeInsets.all(8.0),

                child: FutureBuilder<ApiResult>(
                  future: fetchWeatherByCountry(countries[index].countryName),
                  builder:( context,  snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: <Widget>[
                          Text( '${countries[index].countryName}' , style: TextStyle(color: Colors.black , fontSize: 20.0),),

                          Row(

                            children: <Widget>[
                              Text(snapshot.data.weather[0].description, style: TextStyle(color: Colors.black , fontSize: 20.0),),
                              Padding(padding: EdgeInsets.all(3.0)),
                              Text('${snapshot.data.main.temp.toString()}F', style: TextStyle(color: Colors.black , fontSize: 20.0),)

                            ],
                          ),
                          Image.network(
                              'http://openweathermap.org/img/w/${snapshot.data.weather[0].icon}.png'
                          )

                        ],
                      );
                    }
                    else{
                      return CircularProgressIndicator();
                    }
                  }
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSeparator(context, index) {
    return VerticalDivider(
      width: 1,

      color: Colors.white,
    );
  }
}