import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_app/models/api_model.dart';



  String baseUrl = 'http://api.openweathermap.org/data/2.5/weather?q=';
      String APPID ='&APPID=9037c569053952904642f3ee16e99242';
  final successCode =200;
  Future< ApiResult > fetchWeatherByCountry(String Country) async {
    final response =  await http.get('${baseUrl}${Country}${APPID}');
    final ResponseString  = jsonDecode(response.body);
    if ( response.statusCode == successCode){
      return ApiResult.fromJson(ResponseString);
    }
    else{
      throw Exception ('failed to load');
    }


  }


