import 'package:flutter/material.dart';
import 'package:flutter_app/Themes/themes.dart';
import 'package:flutter_app/router.dart' as prefix0;
import 'package:flutter_app/services/Respository.dart';
import 'package:flutter_app/widgets/horizontal_bar.dart';
import 'package:flutter_app/widgets/Nav_Bar.dart';
import 'package:flutter_app/router.dart';




const String citiesRoute = prefix0.citiesRoute;

class Homepage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
          'Weather App',
          style: appBarTextStyle,
        )),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 8.0),
          HorizontalBar(),
          SizedBox(height: 8.0),
          NavBar(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(
              context, prefix0.addCity);
        },
        child: Icon(Icons.add),
        tooltip: 'Add a new city',
      ),
      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          elevation: 20.0,
          items: [
            BottomNavigationBarItem(
              title: Text('Search'),
              icon: Icon(Icons.search),
            ),
            BottomNavigationBarItem(
                title: Text('My Cities'),
                icon: IconButton(
                  icon: Icon(Icons.location_city),
                  onPressed: () {
                    Navigator.pushNamed(context, citiesRoute);
                  },
                )
            ),
          ],
        ),
      ),
    );
  }
}
