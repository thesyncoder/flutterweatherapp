

import 'package:flutter_app/models/api_model.dart';

abstract class WeatherListingState{}

class WeatherUnintializedState extends WeatherListingState{}

class WeatherFetchingState extends WeatherListingState{}

class WeatherFetchedState extends WeatherListingState{
  final List<Weather> weather;
  WeatherFetchedState( {this.weather}):assert(weather!=null);


}

class WeathErrrorState  extends WeatherListingState{}

class WeatherEmptyState extends WeatherListingState{}
